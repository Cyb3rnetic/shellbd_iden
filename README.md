# Shell and Backdoor Identifier

A quick dirty script to be extended by the user to aid in shell and backdoor identification


## Python Versions

Runs on Python 2.x and 3.x.


## How To Run

[1] Set configurations in scan() function

[2] Upload to remote website

[3] python shellbd_iden.py
