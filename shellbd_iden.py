#!/usr/bin/python

__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft 2017 Cyb3rnetic'
__license__ = 'Open Source'


"""
SHELLBD_IDEN.PY

Shell and Backdoor Identifier

This tool simply attempts to identify malicious files, content, backdoors and shells
that I have encountered while performing security consulting services on websites.
It also detects 132+ additional backdoors and shells found in the wild.

This script must be uploaded and ran locally on said server.
You usually want to place this one level above your public_html > SEE CONFIGURATIONS

You may have to handle your own false positives as they are discovered or come
into conflict with strings used to identify malicious content and files.
"""


import os
import time
import sys
import signal
from datetime import datetime


"""
color

Color class
"""


class color:
    """
    color

    Color class
    """
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


def banner():
    """
    banner

    Script intro and information banner
    """
    print("\n\n")
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.BLUE + ("*" * 70)  + ""+ color.ENDC)
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.GREEN + " Shell and Backdoor Identifier"+ color.ENDC)
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.GREEN + " Version 0.0.5 Code Name: GoGoGadget Identifier!"+ color.ENDC)
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.GREEN + " Developed by : Cyb3rnetic <@h4cklife>"+ color.ENDC)
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.BLUE + ("*" * 70)  + ""+ color.ENDC)


def main():
    """
    main

    Call banner and wait for user input before proceeding with scan.
    """
    signal.signal(signal.SIGINT, signal_handler)
    banner()
    if sys.version_info[0] < 3:
        userin = raw_input("\n" + color.BLUE + "[" + color.GREEN + "+" + color.BLUE + "] " + color.WARNING + "Please press ENTER to continue" + color.ENDC)
    else:
        userin = input("\n"+color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.WARNING + "Please press ENTER to continue"+ color.ENDC)


    while True:
        scan()


def scan():
    """
    scan

    Initiate scan sequence
    """
    # CONFIGURATIONS
    FIINJCK     = 1         # Check for file injections ; enabled by default
    ERRLOG      = 1         # Error logging is enabled by default
    TIMVER      = '2.8.14'  # Latest timthumbs version (keep this updated)
    SLEEP       = 300       # How long should the scanner sleep before perfoming another 2600 = 1hr | 1800 = 30minutes | 300 = 5minutes
    SLEEP_STATUS= 0         # 0=Off 1=On ; disabled by default

    # DO NOT EDIT
    COUNT       = 0      # Number of files
    FOUND       = 0      # Found vulnerabilities
    LOG         = ""     # Log results
    z           = 0      # Our count status
    
    FILENAME    = "shellbd_iden.py"
    SCANDIR     = "/var/www/html/" # ./public_html
     
    # Shellprints are strings scraped from known shells and malicious files
    SHELLPRINTS = ["ir_a_bajo()", "SyR!4N", "ReZK2LL", "WSO_VERSION", "uname -a; w; id; /bin/sh -i", "c999shvars", "YohUhQIAOw==", "$proxy_shit", "x8HNZsxwbVVj/Dw==",
    "wolf hacker", "o_0_h@gmail.com", "TN_X2X", "$c99sh_sourcesurl", "WICxe()", "srv_info($title,$contents)", "$tabledump", "/tmp/angel_bc", "name='mip'", 'request.querystring("klas")',
    "$apiserv = 'h'.'t'.'tp'", 'type="file" name="pictures[]"', "/etc/passwd", "Webshell", 'content="phpshell"',
    "SultanMehmed", "w0lf", "Captain Crunch Team", "physx", "Preddy", "BloodSword", "Snailsor", "Cnqing", "alikaptanoglu", "Kacak", "TurkGuvenligi", "GrayHatz", "zehir", "PHP_Shell_Options",
    "CgpOwo=", "KioqKioqKioq", "readzlib", "searchtableform", "Ci8vU3RhcnRpbmcgY", 'TYPE="hidden" name="MAX_FILE_SIZE"', "Iy0tQ29uZmlnLS0", 'dim targetPath,cp_dst,mv_dst,root',
    'ParseDisposition', '1A7023', 'corfundotabela', 'cmd.exe', 'BgToggle', 'CURLOPT_URL, "file:file:///"', 'Turkish Security Network', 'KingDefacer', 'q1w2e3r4', 'passthru(getenv("HTTP_ACCEPT_LANGUAGE"));', "runcommand('canirun'", 'ironwarez', 'milw0rm', 'shellhelp', 'cgitelnet', 
    'Grinay', 'AK-74', 'ak74-team', 'Vela iNC', 'Ayyildiz Tim', 'Milletinin', '%s?work_dir=%s', 'fonksiyonlar', 'haklar', '"".$dbname."-".$date."_sql.tar.gz"', 
    'c0derz', 'web-shell', 'shellexec', 'session("shagman")', "objFSO.DeleteFile yol", "@$_POST['php_eval']", "Locus7", "Captain Crunch", "physx", "Preddy", 
    "c99sh_surl", "fwrite($dosya, $metin)","Thehacker", "Agd_Scorp", "Cr@zy_King", "KinSize", "JeXToXiC", "s3f4", '$curcmd = "cd ".$curdir.";".$curcmd', 'cat /etc/', "Loader'z",
    "bind(S, sockaddr_in", "xhackers.txt", "$loli", "MAX666", "iranstars.com", '@is_uploaded_file("$dir/$file")', '%s?work_dir=%s">%s', "Wardom", "inclink('dlink', 'basepw')",
    '@copy($file, "./$file_name")', 'Moroccan', 'GhOsT', '$PHP_SELF?action=dropDB&dbname=$dbname', '$MyShellVersion', 'mysql_web_admin_username', 'dropDatabase', 'new RestoreTool',
    'ALTER TABLE $tablename ADD PRIMARY KEY( $primary )', '.:NCC:.', 'National Cracker', 'z0mbie', "d=$d/$dir'", "posix_getuid", "NTDaddy", 'request.servervariables("SERVER_PROTOCOL")', 
    'findsock', "phpshellhost", "system(\$_GET[cpc]);exit;", 'phpRemoteView', 'realm=\"phpRemoteView\"', '@extract($_POST, EXTR_SKIP)', 'Macker', 'cmd=deldir', 
    'unlink("$dizin/$sildos")', '$dez = $pwddir', 'ConvertBinary(ByVal SourceNumber', 'Mehdi', 'HolyDemon', 'FAAAAA==G    PnMDKDPM+k', 'isLinux', 'sitemap: http://cdn.attracta.com/sitemap/2519186.xml.gz',
    "@$output = include($_POST['incl']);", "Ru24PostWebShell", 'id;pwd;uname -a;ls -la', "Cr@zy_King", '$filename = $backupstring."$filename";', 'Safe0ver', 'Evilc0der', '$SFileName=$PHP_SELF', 
    '$zrodlo = fopen($temp, "r");', 'Shell_Exec($cmd)', 'system($cmd)', 'SimShell', '<form name="shell"', '$sedat=@opendir("/tmp")', '$tymczas', '(substr($root,', 'Server.CreateObject("Scripting.Dictionary")',
    'name="MAX_FILE_SIZE" value="100000"', 'move_uploaded_file($userfile, $serverfile)', "type='submit' name='sql' value='Connect'",
    "Rohitab Batra", "Trying $ServerName...", "CGI-Telnet", "Maceo", 'szCMD = Request.Form(".CMD")', 'curl_setopt($ch, CURLOPT_URL, "http://$host:2082");',
    "Arab Security", "Super-Crystal", "Mohajer22", "sup3r-hackers", "Medo-HaCKer", "Anaconda", "ReeM-HaCK", "NoOFa", "AL-Alame", "The YounG HackeR", "Anti-Hack", "Pixcher", "Cyber Lords",
    "$myports = array(", "anonim_mail", "Bl0od3r", "Netplayazz", "Dive.0.1", "Emperor Hacking", "iM4n", "FarHad", "imm02tal",
    'file_exists("/usr/', 'execl("/bin/sh"', "netstat", "callfuncs('find .", "Gamma Group", "push @$chunks, @{$self->evaluate_operation",
    "g00nshell", "execphp", "Bomber", "GFS Web-Shell", "kalabanga", '$param{pwd} ne $pwd){print "user invalid', "cribble.by.ru", "h4ntu", "tsoi", 'system("$cmd 1> /tmp/cmdtemp',
    'UgsAAA==', '?p=rename&file=', "KA_uShell", '$wser = "whois.ripe.net"', "JspWebshell", 'action=del&path=',
    'aktifklas', "Kodlama", "BLaSTER", "TurkGuvenligi", "?option=com_user&view=reset&layout=confirm",'$perms .= ($mode & 00100', 'd1 = Request("dosya1")', 'if d1<>"" then d1 = true', 'zyklonshell']
    
    
    # Evilprints are strings in files to be careful of and pay attention too
    # * Could cause false positives but rare on common html, php, js, and css files
    # * Should be inspected and verified to be malicious manually
    EVILPRINTS = ["eval (base64_decode (", "preg_replace($exif", "shell_exec", '@rmdir($deldir)', 
    '$perms & 0x0100', "@extract($_REQUEST)", 'Request.Form("cmd")="Download"', 
    "@phpinfo();", 'ls -la', 'uname -a']
    
    # Evilfiles are file names that expose system information, are known to be used as malicious tools or left behind by malicious users
    # * Should be inspected and verified to be malicious or public facing manually
    EVILFILES = ["cookie.txt", "pass.txt", "timthumb.php", "timthumbs.php", "thumbnail.php", "shells.txt", "Commands.php",
    "uploader.php", "version.txt", "phpinfo.php", "test.php", "eval.php", "evil.php", "x2300.php", "casus15.php", "cgitelnet.php", "CmdAsp.asp",
    "dingen.php", "entrika.php", "529.php", "accept_language.php", "Ajax_PHP_Command_Shell.php", "AK-74", "Antichat_Shell" "Antichat Shell", "antichat.php",
    "aspydrv.php", "ayyildiz.php", "azrailphp.php", "b374k.php", "backupsql.php", "c0derz_shell.php", "c0derzshell", "c99", "locus7", "madnet.php", 
    "madshell", "casus.php", "cmdasp", "cpanel.php", "crystalshell", "cw.php", "cybershell.php", "dC3.php", "diveshell", "dive.php", "dtool", "erne.php", "ex0shell",
    "fatal.php", "findsock", "ftpsearch", "g00nshell", "gamma", "gfs", "go-shell", "h4ntu", "ironshell", "kadot", "ka_ushell", "kral.php", "klasvayv",
    "lolipop", "Macker", "megabor", "matamu", "lostdc", "myshell", "mysql_tool.php", "mysql_web", "NCC-Shell", "nshell", "php-backdoor", "PHANTASMA", "spy", "phpinj",
    "predator", "pws.php", "qsd-php", "reader.asp", "ru24", "safe0ver","rootshell", "RemExp", "simattacker", "simshell", "simple-backdoor", "sosyete", "soldierofallah", 
    "small.php", "stres.php", "tryag.php", "toolaspshell", "stnc", "sincap", "winx", "Upload", "zaco", "zehir", "zyklon" ]
    
    # Hashprints are eval base64 string injections and malicious content snippets found in modified site files
    # These strings are commonly found pasted into the first or last line of a site file
    # * Could cause false positives but rare on common html, php, js, and css files
    # * Should be inspected and verified to be malicious manually
    HASHPRINTS = ["FVjMmNpS1NrN0lBPT0iKSk7IA", "7b2HmtvIsSj8BH4HHl79luZwJESm1Uo", "vLLaGErGwhkJ4QEghbIAkJcE4", "H4sIALMXx0QAA+RafXhU1Zm", "f0VMRgEBAQAAAAAAAAAAAAIAAwABAAAA2IUECDQAAA", "IyEvdXNyL2Jpbi9wZXJsDQp1c2UgU29",
    "f0VMRgEBAQAAAAAAAAAAAAIAAwABAAAAoIUECDQAAA", "HJ3HkqNQEkU/ZzqCBd4t8V4YAQI2E3j", "Pz48c2NyNHB0IGwxbmczMWc1PWoxdj", "IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMj", "4AAQSkZJRgABAQEAYABgAAD",
    "R0lGODlhFAAUALMIAAD", "I2luY2x1ZGUgPHN0ZGlvLmg", "IyEvdXNyL2Jpbi9wZXJsDQokU0hFTE", "IyEvdXNyL2Jpbi9wZXJsDQp1c2UgU29j", "I2luY2x1ZGUgPHN0ZGlvLmg", "IyEvaG9tZS9tZXJseW4vYmluL", "XCI7DQoNCnN1YiBwcmVmaXggew0KIG1",
    "QX2oZ2VTDpo6g3FYAa6X"]
    
    # Extension that should not be floating around in the public web space.
    EXTENSIONS = [".sql"]
    
    # Announce time now
    startTime = datetime.now()
    print(color.BLUE + "\n\n["+color.GREEN+"+"+color.BLUE+"] " + color.FAIL + " Time now : " + color.ENDC + str(startTime))
    
    # Count the number of files recursively in the current directory and print a status message
    for root, dirs, files in os.walk(SCANDIR):
        for i in files:
            COUNT = COUNT + 1
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.FAIL + " Server File Count : " + color.ENDC + str(COUNT))
    
    # Announce scan launch
    print("\n\n" + color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.WARNING + "Beginning scan sequence...\n\n"+ color.ENDC)
    
    # Begin scan on counted files
    for root, dirs, files in os.walk(SCANDIR):
        for i in files:
            z = z + 1
            #sys.stdout.write(color.BLUE + "\r["+color.GREEN+str(z)+color.BLUE+"/"+color.GREEN+str(COUNT)+color.BLUE+"]"+ color.ENDC)
            sys.stdout.flush()
                
            if FILENAME not in i:
                # Try to open the file and store its contents to a var
                try:
                    CONTENTS = open(root + "/" +i).read()
                except:
                    if ERRLOG == 1:
                        LOG = "\n"+color.BLUE + "["+color.FAIL+"-"+color.BLUE+"] " + color.FAIL + "ERROR READING: " + root + "/" +i+ color.ENDC
                        sys.stdout.write(LOG+"\n")
                        sys.stdout.flush()
                
                FOUND = FOUND + injections(FIINJCK, CONTENTS, HASHPRINTS, i, root)
                FOUND = FOUND + shell_prints(CONTENTS, SHELLPRINTS, i, root)
                FOUND = FOUND + evil_prints(CONTENTS, EVILPRINTS, i, root)
                FOUND = FOUND + evil_files(CONTENTS, EVILFILES, i, root)
                FOUND = FOUND + extensions(CONTENTS, EXTENSIONS, i, root)

    # Scan is now complete
    complete_scan(startTime, FOUND, SLEEP_STATUS)
    if SLEEP_STATUS == 1:
        time.sleep(SLEEP) # 2600 = 1hr | 1800 = 30minutes | 300 = 5minutes
    else:
        exit(0)


def md5sum(filename, blocksize=65536):
    """
    md5sum

    @param filename: 
    @param blocksize: 
    @return: 
    """
    hash = hashlib.md5()
    with open(filename, "r+b") as f:
        for block in iter(lambda: f.read(blocksize), ""):
            hash.update(block)
    return hash.hexdigest()


def injections(FIINJCK, CONTENTS, HASHPRINTS, i, root):
    """
    injections

    @param FIINJCK: 
    @param CONTENTS:
    @param HASHPRINTS: 
    @param i:
    @param root:
    @return:

    Attempt to identify hashprints
    Commonly seen as base64 injections to the top or bottom of site files
    """
    # If injection check is enabled (FIINJCK) check for hashprints; strings that have been identified as malicious
    tmp = 0
    if FIINJCK == 1:
        for HASHPRINT in HASHPRINTS:
            if HASHPRINT in CONTENTS:
                LOG = color.BLUE + "[" + color.GREEN + "+" + color.BLUE + "] " + color.ENDC + color.FAIL + "IDENTIFIED" + color.ENDC + " : " + color.FAIL + "HASHPRINT" + color.ENDC + " : " + root + i + " -> " + HASHPRINT + color.ENDC
                sys.stdout.write(LOG + "\n")
                sys.stdout.flush()
                tmp = tmp + 1
    return tmp


def evil_files(CONTENTS, EVILFILES, i, root):
    """
    evil_files

    @param CONTENTS:
    @param EVILFILES: 
    @param i:
    @param root:
    @return:

    Attempt to identify evil files
    """
    tmp = 0
    for EVILFILE in EVILFILES:
        if EVILFILE in i:
            # Added support for checking WordPress TimThumb files for most recent version
            if EVILFILE == "timthumb.php" or EVILFILE == "timthumbs.php" or EVILFILE == "thumbnail.php":
                if TIMVER not in CONTENTS:
                    LOG = "\n"+color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.ENDC + color.FAIL+"IDENTIFIED"+color.ENDC+" : "+color.FAIL+"EVILFILE"+color.ENDC+" :" + root +i+ " -> " +EVILFILE+ color.ENDC
                    sys.stdout.write(LOG+"\n")
                    sys.stdout.flush()
                    tmp = tmp + 1
            # If this is not a timthumb file continue as normal
            # Attempt to handle known false positives
            elif ("Diff/Engine/shell" not in i) and ("mysql_test.php" not in i and EVILFILE not in "test.php") and ("soundcloud-shortcode-test.php" not in i and EVILFILE not in "test.php"):
                LOG = color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.ENDC + color.FAIL+"IDENTIFIED"+color.ENDC+" : "+color.FAIL+"EVILFILE"+color.ENDC+" :" + root +i+ " -> " +EVILFILE+ color.ENDC
                sys.stdout.write(LOG+"\n")
                sys.stdout.flush()
                tmp = tmp + 1
    return tmp


def evil_prints(CONTENTS, EVILPRINTS, i, root):
    """"
    evil_prints

    @param CONTENTS
    @param EVILPRINTS: 
    @param i:
    @param root:
    @return: 

    Attempt to identify evilprints ; strings and functions commonly seen in malicious code
    """
    tmp = 0
    for EVILPRINT in EVILPRINTS:
        if EVILPRINT in CONTENTS:
            # Attempt to handle known false positives
            if (".mp3" and ".psd" and ".jpg" and ".png" not in i) and ("used internally by Diff" not in CONTENTS) and ("MCManager.includes" not in CONTENTS) and ("HMBKP_Requirements" not in CONTENTS) and ("HM_Backup" not in CONTENTS) and ("contact-form-7" not in i and EVILPRINT not in "Mehdi") and ("class-wp-filesystem-base.php" not in i and EVILPRINT not in "$perms & 0x0100"):
                LOG = color.BLUE + "[" + color.GREEN + "+" + color.BLUE + "] " + color.ENDC + color.FAIL + "IDENTIFIED" + color.ENDC + " : " + color.WARNING + "EVILPRINT" + color.ENDC + " :" + root + i + " -> " + EVILPRINT + color.ENDC
                sys.stdout.write(LOG + "\n")
                sys.stdout.flush()
                tmp = tmp + 1
    return tmp


def shell_prints(CONTENTS, SHELLPRINTS, i, root):
    """
    shell_prints

    @param CONTENTS
    @param SHELLPRINTS: 
    @param i:
    @param root:
    @return:

    Attempt to identify shellprints
    """
    tmp = 0
    for SHELLPRINT in SHELLPRINTS:
        if SHELLPRINT in CONTENTS:
            # Attempt to handle known false positives
            if (".mp3" and ".psd" and ".jpg" and ".png" not in i) and (("wp-db-backup-tr_TR" not in i) and ("haklar" not in SHELLPRINT)) and (("dashicons" not in i) and ("tsoi" not in SHELLPRINT)) and (("captcha-bn_BD" not in i) and ("Mehdi" not in SHELLPRINT)):
                LOG = color.BLUE + "[" + color.GREEN + "+" + color.BLUE + "] " + color.ENDC + color.FAIL + "IDENTIFIED" + color.ENDC + " : " + color.BLUE + "SHELLPRINT" + color.ENDC + " :" + root + i + " -> " + SHELLPRINT + color.ENDC
                sys.stdout.write(LOG + "\n")
                sys.stdout.flush()
                tmp = tmp + 1
    return tmp


def extensions(CONTENTS, EXTENSIONS, i, root):
    """
    extensions

    @param CONTENTS: 
    @param EXTENSIONS: 
    @param i: 
    @param root: 
    @return:

    Check for extensions that should not be in the public web space
    """
    tmp = 0
    for EXTENSION in EXTENSIONS:
        if EXTENSION == i[-4:]:
            LOG = color.BLUE + "[" + color.GREEN + "+" + color.BLUE + "] " + color.ENDC + color.FAIL + "IDENTIFIED" + color.ENDC + " : " + color.WARNING + "EXTENSION" + color.ENDC + " : " + root + i + " -> " + EXTENSION + color.ENDC
            sys.stdout.write(LOG + "\n")
            sys.stdout.flush()
            tmp = tmp + 1
    return tmp


def complete_scan(startTime, FOUND, SLEEP_STATUS):
    """
    complete_scan

    @param startTime: 
    @param FOUND: 
    @param SLEEP_STATUS: 
    @return:

    Complete scan inforamtion banner and statistics
    """
    print("\n\n") 
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.FAIL + " Started Time : " + color.ENDC + str(startTime))
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.FAIL + " Ended Time : " + color.ENDC + str(datetime.now()))
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.FAIL + " Elapsed Time : " + color.ENDC + str(datetime.now()-startTime))
    print(color.BLUE + "["+color.GREEN+"+"+color.BLUE+"] " + color.FAIL + " Indentified : " + color.ENDC + str(FOUND)+ color.ENDC)
    print("\n")
    if SLEEP_STATUS == 1:
        print("Sleeping...")


def signal_handler(signal, frame):
    """
    signal_handler

    @param signal:
    @param frame:

    Exit using CTRL+C
    """
    print(' Exiting!')
    sys.exit(0)


if __name__ == "__main__":
    """
    Initialization

    If running as a script and not as an import call main()
    """
    main()